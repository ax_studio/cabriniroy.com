"use strict";



$( window ).on( "load", function() {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 600) {
            $('.scrolltotop').fadeIn(300);
        } else {
            $('.scrolltotop').fadeOut(500);
        }
    });


    $("#slideup").click(function () {
        $('html, body').animate({
            scrollTop: $("#top").offset().top
        }, 1000);
    });

});